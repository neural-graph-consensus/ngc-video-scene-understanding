# NGC video scene understanding

## Scope
The purpose of this repository is to extend the work done in the original [NGC repository](https://gitlab.com/neural-graph-consensus/semisup-multitask-scene-understanding) (which supports only CARLA data) to any video of any scene.

In order to extract the representations/layers, we use the [Video Representations Extractor](https://gitlab.com/neural-graph-consensus/video-representations-extractor) repository, which offers various built-in representations as well as the ability to create custom ones, based on the scene, such as pretrained semantic segmentation or fine tuned depth estimation.

The extractor repository represent the first iteration in the original paper. The next iterations are computed automatically on the same data (no additional data can be provided for now) after the graph ensembles are created. The process repeats until a fixed number of iterations.

The video representations CFG is in the same format we use in the original repository ([example](https://gitlab.com/neural-graph-consensus/video-representations-extractor/-/blob/master/cfgs/testCfg_ootb.yaml)).

The graph CFG file is in the same format as in AAAI repository ([example](https://gitlab.com/neural-graph-consensus/semisup-multitask-scene-understanding/-/blob/master/cfgs/carla/graph_paper.yaml)).

Currently we use the nodes defined in the original repository ([here](https://gitlab.com/neural-graph-consensus/semisup-multitask-scene-understanding/-/tree/master/model/nodes)). If more special nodes/layers are used they must be defined here and properly initialized before passing to the `NGC` object in order for the edges to be instantiated.

Each node also has a data reader in the [ngclib](https://gitlab.com/neural-graph-consensus/ngclib), which is defined [here](https://gitlab.com/neural-graph-consensus/ngclib/-/blob/master/ngclib/readers/ngc_npz_reader.py), so adding new nodes implies this reader must also be maintained. The best way to add a new layer is to submit a PR to the `ngclib`.

## 1. Training step

For the training process, see [this](example/example.md).

##  2.Evaluation step

Upon training, you should have, under `ngcDir` all the models and pseudolabels for all defined iterations. These models can be used to get the evaluation metrics on new testing data, with ground truth, to validate the generalization process of the NGC and of the models trained with semisupervised data on later iterations.

### 2.1 Checking ngc dir training status
To check that the graph is indeed trained, so that we can export metrics, use:

```
python main_check_ngcdir.py --ngcDir ngcDir/ --graphCfgPath graph.yaml
```

Upon calling this, you should get an output for all edges, as defined by `graph.yaml` and for all iterations as well as their training status.

### 2.2 Exporting predictions on an evaluation set

This step is required to get offline metrics for our graph, at each iteration. To make things simple, we can use the validation set, but any dataset with ground truth available can be used for evaluation. This step can also be used for visualisation purposes on semisupervised sets, where only the input nodes are available.

We have to repeat these step for each iteration. If we have defined multiple voting functions, we can run the same script on different `graph.yaml`, which contain the same edges, but different voting schemes. Existing predictions will no be overwritten.

```
python main_export_predictions.py --ngcDir ngcDir/ --graphCfgPath graph.yaml --datasetPath /path/to/evaluation_set --outputDir /path/to/output_dir --iteration N (1, 2, etc.)
```

Upon calling this, in `/path/to/output_dir` you will have a structure of directories, containing, for each node, the predictions based on the voting algorithm, as well as all the single link predictions.

For example, if we were to have a node, called `nodeX`, then under `/path/to/output_dir/nodeX`, we'd have the following strucutre:

```
/path/to/output_dir/nodeX
  /simpleMean [example of voting algorithm]
    1.npz, ..., N.npz
  /simpleMedian [example of voting algorithm]
    1.npz, ..., N.npz
  /singleLinks
    nodeY [example of single link or intermediate]
      1.npz, ..., N.npz
    nodeZ [example of single link or intermediate]
      1.npz, ..., N.npz
```

Basically, under `simpleMedian`, we have all the predictions of the `NGC` graph, using the median of all links towards `nodeX`. Under `singleLinks/` we have all the individual links, without any voting (either `inputNode->nodeX` or `inputNode->nodeY->nodeX`).

### 2.3 Getting evaluation metrics

Based on the step 2.2, for each node, we can extract the metrics, given a GT set.

```
python main_metrics_loco.py --predictionDir /path/to/output_dir/nodeX/singleLinks/nodeY --gtDir /path/to/evaluation_set/nodeX --nodeName nodeX --graphCfgPath graph.yaml
```

This will give you all the metrics for the node `nodeX` based on it's type as defined in `graph.yaml` and implemented in code in the class of the node's type.

The result will be concatenated in `nodeX.txt` in the current directory. Each entry should be of the form:
```
{'Loss': score, 'Metric1': score, 'Metric2': score, 'nodeName': 'nodeX', 'which': 'nodeY'}
```
Line represents the result of `inputNode->nodeY->nodeX` or `nodeY->nodeX` if `nodeY` is the input node of the graph.

### 2.4 Exporting image samples

We can export a collage of samples based on a trained `ngcDir`, so we can visualize each single link and each voting function. We will use the same output of 2.2.

For each node:
```
python main_make_collage.py  --predictionsDir /path/to/output_dir --graphCfgPath graph.yaml --nodeName nodeX --outputDir /path/to/collage_dir --rgbDir /path/to/evaluation_set/rgb/ --gtDir /path/to/evaluation_set/nodeX
```

This will yield, under `/path/to/collage_dir`, a set of `.png` images, one for each input file (RGB, GT and predictions, both single links and voting ones).

Finally, in order to make a video out of them, we can use:
```
cd /path/to/collage_dir/nodeX
ffmpeg -start_number 0 -framerate 10 -i %d.png -c:v libx264 -pix_fmt yuv420p nodeX_collage.mp4
```