import pandas as pd
import sys
import ast

def main():
    raw_data = open(sys.argv[1], "r").readlines()
    data = []
    for line in raw_data:
        data.append(ast.literal_eval(line))

    cols = list(data[0].keys())

    res = []
    _cols = ["Loss", "Accuracy", "F1 Score (mean)", "Mean IoU (mean)"]
    for row in data:
        _row = [row[col] for col in _cols]
        res.append(_row)
    res = pd.DataFrame(res, columns=_cols)
    res.to_csv("res_avgs.csv", float_format="%.3f")

    f1_data = [x["F1 Score"] for x in data]
    f1_data = pd.DataFrame(f1_data, columns=["land", "forest", "residential", "road", "little-objects", "water", "sky", "hill"])
    f1_data.to_csv("res_f1.csv", float_format="%.3f")

    miou_data = [x["Mean IoU"] for x in data]
    miou_data = pd.DataFrame(miou_data, columns=["land", "forest", "residential", "road", "little-objects", "water", "sky", "hill"])
    miou_data.to_csv("res_miou.csv", float_format="%.3f")


if __name__ == "__main__":
    main()

