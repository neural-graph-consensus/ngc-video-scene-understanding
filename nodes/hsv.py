from nwgraph import MapNode
from ngclib_cv.nodes import HSV as _HSV
from .models import EncoderMap2Map

class HSV(_HSV):
    def get_encoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.num_dims)

    def get_decoder(self, inputNode):
        assert False, "HSV cannot be decoded in this project."
