from ngclib_cv.nodes import RGB as _RGB
from .models import EncoderMap2Map, EncoderMap2MapNoResidual, DecoderMap2Map
from .depth_no_residual import DepthNoResidual
from nwgraph import MapNode

class RGB(_RGB):
    def get_encoder(self, outputNode):
        if isinstance(outputNode, DepthNoResidual):
            return EncoderMap2MapNoResidual(dIn=self.num_dims)
        return EncoderMap2Map(dIn=self.num_dims)

    def get_decoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2Map(dOut=self.num_dims)
