### Steps to generate and train train on random data

1. Generate random data

Using [this](https://gitlab.com/neural-graph-consensus/ngclib/-/blob/master/helper_scripts/main_generate_random_data.py) script, we'll generate random data for 3 nodes (RGB, Depth and Semantic) as it would come from the layers extraction process.

```
python3 main_generate_random_data.py --baseDir layersDir --representations rgb,depthRand,semanticRand --representationTypes float,float,categorical --representationDims 3,1,12 --resolution 240,426 --N 100
```

2. Split the data in train, validation and semisupervised using the provided script from [here](https://gitlab.com/neural-graph-consensus/ngclib/-/blob/master/helper_scripts/main_split_layers.py).

```
python3 main_split_layers.py --layersDir layersDir --outDir layersDirSplit --percentages 70,10,20
```

3. Define the graph cfg

```
graphConfig: NGC
nodeTypes: [RGB, Semantic, Depth]
nodeNames: [rgb, semanticRand, depthRand]
edges: [ # Edges are defined using the names of the nodes
    rgb_semanticRand,
    rgb_depthRand,
    depthRand_semanticRand 
]
hyperParameters:
  semanticRand:
    semanticClasses: 12
    semanticColors: [
      [0, 0, 0],
      [0, 0, 1],
      [0, 0, 2],
      [0, 0, 3],
      [0, 0, 4],
      [0, 0, 5],
      [0, 0, 6],
      [0, 0, 7],
      [0, 0, 8],
      [0, 0, 9],
      [0, 0, 10],
      [0, 0, 11],
    ]
  depthRand:
    maxDepthMeters: 1
inputNode: rgb
voteFunction: simpleMean
```

Save this to `graph.yaml`.

4. Define the train cfg

```
seed: 42
batchSize: 10
numEpochs: 3
optimizerType: adamw
optimizerArgs:
  lr: 0.01
patience: 10
factor: 2
```

Save this to `train.yaml`.

5. Train the NGC graph.
```
python3 main.py
    --graphCfgPath graph.yaml
    --trainCfgPath train.yaml
    --trainDir layersDirSplit/train/
    --validationDir layersDirSplit/validation/
    --semisupervisedDirs layersDirSplit/semisupervised/
    --ngcDir ngcDir/
    --numIterations 5 # Change this to desired number of iterations
    [--parallelization 0] # Change this to 1 to use multiple GPUs
    [--debug 0] # Change this to 1 to train only for 3 epochs and 5 iterations per epoch
```

Under `ngcDir` there should be the trained models as well as the exported pseudolabels for each iteration.
