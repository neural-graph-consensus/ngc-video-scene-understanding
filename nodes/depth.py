from nwgraph import MapNode
from ngclib_cv.nodes import Depth as _Depth
from .models import EncoderMap2Map, DecoderMap2Map#Sigmoid as DecoderMap2Map

class Depth(_Depth):
    def get_encoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.num_dims)

    def get_decoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2Map(dOut=self.num_dims)
