"""Kinda useless on its own. Maybe useful to reuse at some point."""
import numpy as np
from media_processing_lib.image import image_read, collage_fn, image_write
from pathlib import Path
import matplotlib.pyplot as plt

def process(img):
    return img[:, 960:1920]

def process_rgb(img):
    return img[:, 0:960]

def process_gt(img):
    return img[:, 1920:]

dirs = ["noaugm", "augm20", "augm50"]
sub_dirs = ["loss", "f1", "last"]
subsubdir = "Train"
imgs = {}
gts = []
rgbs = []
for dir in dirs:
    imgs[dir] = {}
    for sub_dir in sub_dirs:
        imgs[dir][sub_dir] = []
        paths = [str(x.absolute()) for x in Path(f"{dir}/{sub_dir}/{subsubdir}").glob("*.png")]
        pngs = list(map(image_read, paths))
        preds = [process(x.copy()) for x in pngs]
        assert len(preds) > 0, f"{dir}/{sub_dir}"
        imgs[dir][sub_dir] = np.array(preds)

        if dir == "noaugm" and sub_dir == "loss":
            rgbs = np.array([process_rgb(x.copy()) for x in pngs])
            gts = np.array([process_gt(x.copy()) for x in pngs])

for i in range(len(gts)):
    gt = gts[i]
    rgb = rgbs[i]
    noaugm = [imgs["noaugm"][k][i] for k in sub_dirs]
    augm20 = [imgs["augm20"][k][i] for k in sub_dirs]
    augm50 = [imgs["augm50"][k][i] for k in sub_dirs]
    all = [rgb, rgb*0, gt, *noaugm, *augm20, *augm50]
    collage = collage_fn(all, (4, 3))
    image_write(collage, f"{subsubdir}_{i}.png")
