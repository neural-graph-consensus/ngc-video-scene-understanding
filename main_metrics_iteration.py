# TODO: This script expects main_test_vote to be called first and won't care about the edges in graph cfg, just the
#  nodes. Thus, if we have 3 edges for node X, but 5 (+median or w/e selection) were exported for node X, this will
#  report the single link results for all 5 plus the median instead of recomputting them first. 
import ast
import yaml
import numpy as np
import pandas as pd
from argparse import ArgumentParser
from pathlib import Path
from typing import List, Dict
from nwutils.path import get_files_from_dir
from ngclib.utils import load_npz_from_list, getMetrics
from tqdm import tqdm
from main_metrics_loco import getNode

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--graphCfgPath", required=True)
    parser.add_argument("--predictionsDir", required=True)
    parser.add_argument("--gtDir", required=True)
    parser.add_argument("--N", type=int)
    parser.add_argument("--csvOutPath")
    args = parser.parse_args()
    args.gtDir = Path(args.gtDir).absolute()
    args.predictionsDir = Path(args.predictionsDir).absolute()
    if args.csvOutPath is None:
        args.csvOutPath = Path(__file__).parent
    args.csvOutPath = Path(args.csvOutPath).absolute()
    args.csvOutPath.mkdir(exist_ok=True, parents=True)
    return args

def checkDirs(predictionsDir: Path, gtDir: Path, nodeNames: List[str]):
    for name in nodeNames:
        assert (gtDir / name).exists(), f"{gtDir}/{name}"
        assert (predictionsDir / name).exists(), f"{predictionsDir}/{name}"
        assert (predictionsDir / name / "singleLinks").exists(), f"{predictionsDir}/{name}/singleLinks"
        # assert (predictionsDir / name / "selection").exists(), f"{predictionsDir}/{name}/selection"

def getPredDirs(basePredDir:Path) -> Dict[str, Path]:
    singleLinks = {f"singleLinks/{x.name}": x for x in (basePredDir / "singleLinks").iterdir()}
    res = singleLinks
    if (basePredDir / "selection").exists():
        selections = {f"selection/{x.name}": x for x in (basePredDir / "selection").iterdir()}
        res = {**res, **selections}
    else:
        print(f"No selection predictions for '{basePredDir}'.")
    return res

def exportToCsv(df: pd.DataFrame, nodeName: str, outPath: Path):
    print(f"{nodeName}{df}\n_____________________________________")
    outFile = outPath / f"{nodeName}.csv"
    outFileConverted = outPath / f"{nodeName}_converted.csv"
    df.to_csv(outFile)

    def expandColumns(df: pd.DataFrame, colname: str) -> pd.DataFrame:
        df[colname] = df[colname].astype(str).apply(ast.literal_eval)
        df[colname] = df[colname].apply(lambda x : [f"{y:.3f}" for y in x])
        newDf = pd.DataFrame(df[colname].tolist()).add_prefix(f"{colname}_").astype(np.float32).set_index(df.index)
        combined = pd.concat([df, newDf], axis=1).drop(columns=[colname])
        return combined

    for colname in df.columns:
        try:
            df = expandColumns(df, colname)
        except:
            pass
    for column in df.columns:
        try:
            df[column] = df[column].apply(lambda x: f"{x:.3f}")
        except:
            pass
    print(f"Stored at '{outFile}' and '{outFileConverted}'.")
    df.to_csv(outFileConverted)    

def main():
    args = getArgs()
    graphCfg = yaml.safe_load(open(args.graphCfgPath, "r"))
    nodeNames = sorted(list(filter(lambda x: x not in graphCfg["inputNodes"], graphCfg["nodeNames"])))
    print(f"Exporting for output nodes: {nodeNames}.")
    checkDirs(args.predictionsDir, args.gtDir, nodeNames)
    randomize = args.N is not None

    rangeNodes = tqdm(nodeNames)
    for nodeName in rangeNodes:
        rangeNodes.set_description(nodeName)
        nodeRes = []
        node = getNode(graphCfg, nodeName)
        files = get_files_from_dir(args.gtDir / nodeName, pattern="*.npz", randomize=randomize, n_files=args.N)
        gt = load_npz_from_list(files)
        assert len(gt) > 0
        yDirs = getPredDirs(args.predictionsDir / nodeName)
        rangeNodeDirs = tqdm(yDirs.keys())
        for yDir in rangeNodeDirs:
            rangeNodeDirs.set_description(yDir)
            files = get_files_from_dir(yDirs[yDir], pattern="*.npz", randomize=randomize, n_files=args.N)
            y = load_npz_from_list(files)
            assert len(y) == len(gt)
            res = dict(getMetrics(y, gt, node))
            nodeRes.append(res)

        df = pd.DataFrame(nodeRes, index=yDirs.keys())
        exportToCsv(df, nodeName, args.csvOutPath)

if __name__ == "__main__":
    main()
