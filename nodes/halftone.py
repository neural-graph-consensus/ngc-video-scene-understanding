from nwgraph import MapNode
from ngclib_cv.nodes import Halftone as _Halftone
from .models import EncoderMap2Map, DecoderMap2Map

class Halftone(_Halftone):
    def get_encoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.numDims)

    def get_decoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2Map(dOut=self.numDims)
