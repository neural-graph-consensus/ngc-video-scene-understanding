# Script used to export the predictions of an entire iteration of pretrained graph (via ngcDir and graph config) for
#  a new dataset. It will export both the voting ensemble result as well as each individual single link in the output
#  directory.
import yaml
import numpy as np
import torch as tr
from argparse import ArgumentParser
from typing import Dict, List
from lightning_module_enhanced import LightningModuleEnhanced
from nwutils.torch import np_get_data
from ngclib.models import build_model
from ngclib.readers import NGCNpzReader
from ngclib import NGCDir
from ngclib_cv.data import get_data_transform
from pathlib import Path
from tqdm import trange

from main import getNodes

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("--ngcDir", required=True)
	parser.add_argument("--graphCfgPath", required=True)
	parser.add_argument("--datasetPath", required=True)
	parser.add_argument("--outputDir", required=True)
	parser.add_argument("--iteration", default=1, type=int)
	parser.add_argument("--exportSingleLinks", default=1, type=int)
	parser.add_argument("--exportEnsemble", default=1, type=int)
	parser.add_argument("--debug", default=0, type=int)
	parser.add_argument("--num_workers", default=4, type=int)
	args = parser.parse_args()
	args.ngcDir = Path(args.ngcDir)
	args.graphCfg = yaml.safe_load(open(args.graphCfgPath, "r"))
	args.datasetPath = Path(args.datasetPath)
	args.outputDir = Path(args.outputDir)
	return args

def compute_out_files(paths, outputDir, voteDir, i):
	out_files = {}
	for node in paths.keys():
		out_files[node] = []
		for otherNode in paths[node]:
			path = Path(f"{outputDir}/{node}/singleLinks/{otherNode}/{i}.npz")
			out_files[node].append(path)
		# selection
		selectionPath = Path(f"{outputDir}/{node}/selection/{voteDir[node.name]}/{i}.npz")
		out_files[node].append(selectionPath)
	return out_files

def computePaths(y: Dict, inputNodes: List) -> Dict[str, List[Path]]:
	paths = {}
	for node in y.keys():
		if str(node) in inputNodes:
			continue
		yNode = tuple(y[node])[0]
		paths[node] = tuple(x.split(" -> ")[0].split("(")[1] for x in tuple(y[node])[0].path[0].split("|"))
		assert len(paths[node]) == yNode.input.shape[0]
	return paths

def allFilesExist(out_files:Dict[str, List[Path]]) -> bool:
	for node in out_files.keys():
		for file in out_files[node]:
			if not file.exists():
				return False
	return True

def main():
	args = getArgs()
	status = NGCDir(args.ngcDir, args.graphCfg).getStatus()
	assert len(status["iterationInfo"]) >= args.iteration
	iterModelStatus = status["iterationInfo"][args.iteration]["model"]
	total_edges, total_trained = iterModelStatus["totalEdges"], iterModelStatus["totalTrained"]
	assert total_edges == total_trained, f"{total_edges} vs {total_trained}"
	
	nodes = getNodes(args.graphCfg)
	model = build_model(nodes, args.graphCfg)
	weights_dir = args.ngcDir / f"iter{args.iteration}/models"
	model.load_all_edges(weights_dir=weights_dir)
	model.to(device)
	model.eval()
	pl_model = LightningModuleEnhanced(model)
	print(pl_model.summary())

	transforms = {node: get_data_transform(node) for node in nodes}
	reader = NGCNpzReader(path=args.datasetPath, nodes=model.nodes, gt_nodes=model.input_nodes, transforms=transforms)
	print(reader)

	# assert not args.outputDir.exists()
	args.outputDir.mkdir(parents=True, exist_ok=True)
	# Vote dir got more complicated since we added option for model/network selection algorithm.
	voteDir = {}
	for node in model.nodes:
		if isinstance(args.graphCfg["voteFunction"], str):
			voteDir[node.name] = args.graphCfg["voteFunction"]
		elif isinstance(args.graphCfg["voteFunction"], dict):
			voteDir[node.name] = args.graphCfg["voteFunction"][node.name]
		else:
			assert False, f"Not found: {node.name}"
		if isinstance(voteDir[node.name], list):
			assert voteDir[node.name][0] == "networkSelection"
			# Kinda obfuscated. We could read the yaml file and look for modelType as well. 
			modelType = model.voteFn[node.name].model.hyperParameters["modelType"]
			dirName = f"networkSelection_{modelType}"
			voteDir[node.name] = dirName

	x = {k: reader[0]["labels"][k][None] for k in model.input_nodes}
	y = pl_model.np_forward(x)
	paths = computePaths(y, model.input_nodes)

	out_files = None
	N = len(reader) if args.debug == 0 else 5
	for i in trange(N):
		# An optimization to skip some computation if possible by bypassing the model after 1 pass.
		out_files = compute_out_files(paths, args.outputDir, voteDir, i)
		if out_files is not None and allFilesExist(out_files):
			continue

		x = {k: reader[i]["labels"][k][None] for k in model.input_nodes}
		y = pl_model.np_forward(x)

		for node in model.output_nodes:
			assert len(y[node]) == 1
			node_message = list(y[node])[0]
			assert len(paths[node]) == len(node_message.input) == len(out_files[node]) - 1

			# single links
			if args.exportSingleLinks:
				for j in range(len(paths[node])):
					outFile = out_files[node][j]
					if outFile.exists():
						continue
					outFile.parent.mkdir(exist_ok=True, parents=True)
					np.savez(outFile, np_get_data(node_message.input[j]).astype(np.float16))
			# vote
			if args.exportEnsemble:
				outFile = out_files[node][-1]
				if outFile.exists():
					continue
				outFile.parent.mkdir(exist_ok=True, parents=True)
				np.savez(outFile, np_get_data(node_message.output).astype(np.float16))
	print(f"Finished exporting at '{args.outputDir}'.")

if __name__ == "__main__":
	main()
