from nwgraph import MapNode
from ngclib_cv.nodes import Depth as _Depth

import torch.nn as nn
from .models import EncoderMap2Map, DecoderMap2Map

class DepthNoResidual(_Depth):
    def get_encoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.num_dims)

    def get_decoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2Map(dOut=self.num_dims)

# if __name__ == "__main__":
#     model = nn.Sequential(EncoderMap2MapNoResidual(dIn=1), DecoderMap2Map(dOut=3))
#     model = LightningModuleEnhanced(model)
#     x = tr.randn(10, 240, 426, 1).cuda()
#     print(model.summary(input_size=(10, 240, 426, 1)))
#     y = model.forward(x)
#     breakpoint()

