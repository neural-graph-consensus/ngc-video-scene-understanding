from nwgraph import MapNode
from ngclib_cv.nodes import SoftSegmentation as _SoftSegmentation
from .models import EncoderMap2Map, DecoderMap2Map

class SoftSegmentation(_SoftSegmentation):
    def get_encoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.num_dims)

    def get_decoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2Map(dOut=self.num_dims)
