import torch as tr
import numpy as np
from ngclib.trainer.edge_trainer import EdgeTrainer
from ngclib.readers import EdgeReader
from nwmodule.loss import softmax_nll
from pathlib import Path
from main import getArgs, getNodes, getModel

def main():
    args = getArgs()
    nodes = getNodes(args.graphCfg)
    model = getModel(nodes, args.graphCfg)
    print(model.summary())

    model.loadAllEdges(weightsDir=Path("ngcDirV1Complete/iter1/models"), getEdgeDirNameFn=EdgeTrainer.getEdgeDirName)

    edge = model.edges[0]
    edge.eval()
    print(edge)
    edge_reader = EdgeReader(args.validationDir, nodes, edge, edge.getInKeys())
    edge_loader = edge_reader.toDataLoader(batchSize=2, randomize=False, debug=False)
    res = edge.testReader(edge_loader)
    print(res)

    _iter = iter(edge_loader)
    losses = []
    ys, gts = [], []
    y2s = []
    for i in range(len(edge_loader)):
        data = next(_iter)
        x, gt = data["data"], data["labels"]
        y = edge.npForward(x["rgb"])
        l = softmax_nll(tr.from_numpy(y), tr.from_numpy(gt), dim=-1).numpy()
        losses.extend(l)
        ys.extend(y)
        gts.extend(gt)

        y_ngc = model.npForward(x)
        y2 = tuple(y_ngc[getNodes(args.graphCfg)[1]])[0].output
        y2s.extend(y2)

    losses = np.mean(losses)
    print(losses)
    ys = tr.from_numpy(np.array(ys))
    gts = tr.from_numpy(np.array(gts))
    loss2 = softmax_nll(ys, gts).mean().numpy()
    print(loss2)

    y2s = tr.from_numpy(np.array(y2s))
    loss3 = softmax_nll(y2s, gts).mean().numpy()
    print(loss3)
    exit()

if __name__ == "__main__":
    main()
