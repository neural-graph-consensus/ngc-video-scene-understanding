from nwgraph import MapNode
from ngclib_cv.nodes import SoftEdges as _SoftEdges
from .models import EncoderMap2Map, DecoderMap2Map

class SoftEdges(_SoftEdges):
    def get_encoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.num_dims)

    def get_decoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2Map(dOut=self.num_dims)
