import pandas as pd
import sys
import ast

def main():
    raw_data = open(sys.argv[1], "r").readlines()
    data = []
    for line in raw_data:
        data.append(ast.literal_eval(line))

    df = pd.DataFrame(data)
    name = df["nodeName"].iloc[0]
    df = df.drop(columns=["nodeName", "path"])
    for column in df.columns:
        if "(mean)" in column:
            df = df.drop(columns=[column.replace(" (mean)", "")])
    df.to_csv(f"res_avgs_{name}.csv")

if __name__ == "__main__":
    main()

