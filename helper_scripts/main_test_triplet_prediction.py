"""Script that takes two individual predictions (from two similar models) and tests them against the GT"""
import pandas as pd
import numpy as np
from PIL import ImageFont, ImageDraw, Image
from pathlib import Path
from argparse import ArgumentParser
from ngclib.utils import loadNpz
from torch.nn import functional as F
from nwmodule.loss import nll
import torch as tr
from ngclib.utils.node_plot_functions import buildNodePlotFn
from nodes import Semantic, RGB
from sklearn.metrics import f1_score
from media_processing_lib.image import image_write

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--rgb_dir_path", required=True)
    parser.add_argument("--gt_dir_path", required=True)
    parser.add_argument("--pred1_dir_path", required=True)
    parser.add_argument("--pred2_dir_path", required=True)
    parser.add_argument("--X", type=int)
    args = parser.parse_args()
    args.rgb_path = Path(args.rgb_dir_path).absolute() / f"{args.X}.npz"
    args.pred1_path = Path(args.pred1_dir_path).absolute() / f"{args.X}.npz"
    args.pred2_path = Path(args.pred2_dir_path).absolute() / f"{args.X}.npz"
    args.gt_path = Path(args.gt_dir_path).absolute() / f"{args.X}.npz"
    return args

def text(img, text, position, size):
    font = ImageFont.truetype("Roboto-Regular.ttf", size)
    pil_img = Image.fromarray(img)
    draw = ImageDraw.Draw(pil_img)
    draw.text(position, text, font=font)
    new_img = np.array(pil_img)
    return new_img

def min_max(x, min, max):
    return (x - min) / (max-min)

def main():
    args = get_args()
    semantic = Semantic(
        semanticClasses = ["land", "forest", "residential", "road", "little-objects", "water", "sky", "hill"],
        semanticColors = [[0, 255, 0], [0, 127, 0], [255, 255, 0], [255, 255, 255], [255, 0, 0], [0, 0, 255],
            [0, 200, 200], [127, 127, 63]])
    rgb = RGB()

    """Load data"""
    np_rgb = loadNpz(args.rgb_path)
    pred1_np = loadNpz(args.pred1_path)
    pred2_np = loadNpz(args.pred2_path)
    gt_np = loadNpz(args.gt_path).astype(np.int64)

    """some one_hots, argmaxes, softmaxes"""
    pred1_softmax = F.softmax(tr.from_numpy(pred1_np), dim=-1)
    pred2_softmax = F.softmax(tr.from_numpy(pred2_np), dim=-1)
    gt_argmax = tr.from_numpy(gt_np)
    gt_one_hot = F.one_hot(gt_argmax, num_classes=8)
    pred1_argmax = pred1_softmax.argmax(-1)
    pred2_argmax = pred2_softmax.argmax(-1)
    assert (pred1_softmax - pred2_softmax).abs().sum() > 0
    assert (pred1_softmax - gt_one_hot).abs().sum() > 0

    """Confidence on fails"""
    Where_fail1 = pred1_argmax != gt_argmax
    Where_fail2 = pred2_argmax != gt_argmax
    pred1_confidence_fail = (pred1_softmax * Where_fail1[..., None]).max(-1)[0]
    pred2_confidence_fail = (pred2_softmax * Where_fail2[..., None]).max(-1)[0]
    pred1_confidence_fail[(pred1_confidence_fail > 0.01) & (pred1_confidence_fail < 0.5)] = 0.5
    pred2_confidence_fail[(pred2_confidence_fail > 0.01) & (pred2_confidence_fail < 0.5)] = 0.5
    pred1_confidence_fail = min_max(pred1_confidence_fail, 0.5, 1)
    pred2_confidence_fail = min_max(pred2_confidence_fail, 0.5, 1)
    zeros = pred1_confidence_fail * 0

    """Loss / F1 computation """
    pred1_loss = nll(pred1_softmax, gt_one_hot).mean()
    pred2_loss = nll(pred2_softmax, gt_one_hot).mean()
    pred1_f1 = f1_score(pred1_argmax.numpy().flatten(), gt_argmax.numpy().flatten(), average=None,
        labels=range(semantic.numClasses), zero_division=0)
    pred2_f1 = f1_score(pred2_argmax.numpy().flatten(), gt_argmax.numpy().flatten(), average=None,
        labels=range(semantic.numClasses), zero_division=0)
    print(f"Pred1. Loss: {pred1_loss:.3f}. F1: {pred1_f1.mean():.3f}")
    print(f"Pred2. Loss: {pred2_loss:.3f}. F1: {pred2_f1.mean():.3f}")


    """Image stacking"""
    rgb_img = buildNodePlotFn(rgb)(np_rgb)
    pred1_img = buildNodePlotFn(semantic)(pred1_np)
    pred2_img = buildNodePlotFn(semantic)(pred2_np)
    gt_img = buildNodePlotFn(semantic)(gt_np)
    
    pred1_confidence_fail_img = (tr.stack([pred1_confidence_fail, zeros, zeros], dim=-1) * 255).numpy().astype(np.uint8)
    pred1_confidence_fail_img = text(pred1_confidence_fail_img, f"Loss: {pred1_loss:.3f}. F1: {pred1_f1.mean():.3f}", (0, 0), 50)
    pred2_confidence_fail_img = (tr.stack([pred2_confidence_fail, zeros, zeros], dim=-1) * 255).numpy().astype(np.uint8)
    pred2_confidence_fail_img = text(pred2_confidence_fail_img, f"Loss: {pred2_loss:.3f}. F1: {pred2_f1.mean():.3f}", (0, 0), 50)

    stack = np.concatenate(np.concatenate([[rgb_img, gt_img], [pred1_img, pred2_img],
        [pred1_confidence_fail_img, pred2_confidence_fail_img]], axis=1), axis=1)
    image_write(stack, f"res_{args.X}.png")

    res = []
    for i in range(8):
        Where = tr.where(gt_argmax == i)
        _cnt_px = len(Where[0])
        
        gt_class = gt_one_hot[Where].argmax(-1)
        pred1_class_argmax = pred1_softmax[Where].argmax(-1)
        pred2_class_argmax = pred2_softmax[Where].argmax(-1)

        # Compute the TPs
        pred1_tp = (pred1_class_argmax == gt_class)
        pred1_fp = ~pred1_tp
        pred2_tp = (pred2_class_argmax == gt_class)
        pred2_fp = ~pred2_tp
        assert (pred2_fp != (pred2_class_argmax != gt_class)).sum() == 0

        # Compute the number of predictions that are TP on one side and FP on the other
        pred1_better = (pred1_tp & pred2_fp)
        pred2_better = (pred1_fp & pred2_tp)
        
        res.append([_cnt_px, pred1_tp.sum(), pred2_tp.sum(), pred1_better.sum(), pred2_better.sum(),
            pred1_f1[i], pred2_f1[i]])
    res = pd.DataFrame(np.array(res).T, columns=semantic.semanticClasses,
        index=["Cnt", "Pred1 TP", "Pred2 TP", "Pred1 Better", "Pred2 Better", "Pred1 F1", "Pred2 F1"])
    res.to_csv(f"res_{args.X}.csv")
    print(res)

if __name__ == "__main__":
    main()