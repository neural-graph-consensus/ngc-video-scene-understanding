import yaml
import torch as tr
from argparse import ArgumentParser
from pathlib import Path
from typing import List, Dict
from ngclib import NGCDir
from ngclib.models import getModel
from ngclib.nodes.classification_node import ClassificationNode
from nwgraph import Node
from nwutils.pytorch_lightning import PLModule

import sys
sys.path.append(str(Path(__file__).absolute().parent.parent))
from nodes import *
from ngclib.trainer import EdgeTrainer
from main import getNodes

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--graph_cfg_path", required=True)
    # NGC Dir
    parser.add_argument("--ngc_dir", required=True)
    parser.add_argument("--new_ngc_dir", required=True)
    args = parser.parse_args()

    args.ngc_dir = Path(args.ngc_dir).absolute()
    args.new_ngc_dir = Path(args.new_ngc_dir).absolute()
    args.graphCfg = yaml.safe_load(open(args.graph_cfg_path, "r"))
    assert not args.new_ngc_dir.exists(), f"Already exists: '{args.new_ngc_dir}'"
    return args

def main():
    args = getArgs()
    model = getModel(getNodes(args.graphCfg), args.graphCfg)
    print(model.summary())
    ngc_dir_old = NGCDir(args.ngc_dir, args.graphCfg)
    num_iterations = ngc_dir_old.getAvailableIterations()
    print(f"Found {num_iterations} iterations on ngc dir '{args.ngc_dir}'")

    for i in range(num_iterations):
        print(f"Iteration {i+1}")
        weights_dir = Path(f"{args.ngc_dir}/iter{i+1}/models")
        # model.loadAllEdges(weightsDir=weights_dir, getEdgeDirNameFn=EdgeTrainer.getEdgeDirName)
        for edge in model.edges:
            file_name = "model_best_Loss.pkl"
            if isinstance(edge.nodes[1], ClassificationNode):
                file_name = "model_best_F1 Score.pkl"
                weightsFile = weights_dir / EdgeTrainer.getEdgeDirName(edge) / file_name
                if not weightsFile.exists():
                    file_name = "model_best_Loss.pkl"
            
            weightsFile = weights_dir / EdgeTrainer.getEdgeDirName(edge) / file_name
            print(f"Edge: {edge}. Loading weights from '{weightsFile}'.")
            edge.setTrainableWeights(True)
            edge.loadWeights(weightsFile)
        edges = {EdgeTrainer.getEdgeDirName(edge): PLModule(edge.model._module) for edge in model.edges}
        new_dir = args.new_ngc_dir / f"iter{i+1}/models"
        for edge_dir_name, edge in edges.items():
            new_weights_file = new_dir / edge_dir_name / "model_best.pth"
            (new_dir / edge_dir_name).mkdir(exist_ok=False, parents=True)
            print(f"Saved edge '{edge_dir_name}' to '{new_weights_file}'")
            tr.save(edge.state_dict(), new_weights_file)
        break
    print("Done!")

if __name__ == "__main__":
    main()