from functools import reduce, partial
from pathlib import Path
from typing import List, Dict
import yaml
import torch as tr
from argparse import ArgumentParser
from nwgraph import Node
from lightning_module_enhanced import LightningModuleEnhanced
from lightning_module_enhanced.callbacks import PlotCallbackGeneric, PlotMetrics
from nwgraph.node.helper_types import NullNode
from ngclib.logger import logger
from ngclib.models import build_model
from ngclib.trainer import NGCTrainer
from ngclib_cv.data import get_data_transform, get_augmentation, nodes_plot_fn

from nodes import *

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--graphCfgPath", required=True)
    # NGC Dir
    parser.add_argument("--ngcDir", required=True)
    parser.add_argument("--trainCfgPath", required=True)
    # Data level stuff
    parser.add_argument("--trainDir", required=True)
    parser.add_argument("--validationDir", required=True)
    parser.add_argument("--semisupervisedDirs", required=True, help="Split by comma ','")
    parser.add_argument("--startIteration", type=int, default=1)
    parser.add_argument("--endIteration", type=int, default=2)
    parser.add_argument("--debug", type=int, default=0)
    parser.add_argument("--parallelization", type=int, default=0)
    args = parser.parse_args()

    args.ngcDir = Path(args.ngcDir).absolute()
    args.trainDir = Path(args.trainDir).absolute()
    args.validationDir = Path(args.validationDir).absolute()
    args.semisupervisedDirs = [Path(x).absolute() for x in args.semisupervisedDirs.split(",")]
    args.graphCfg = yaml.safe_load(open(args.graphCfgPath, "r"))
    args.trainCfg = yaml.safe_load(open(args.trainCfgPath, "r"))
    return args

def getNodes(cfg: Dict) -> List[Node]:
    nodes = []
    if "edges" in cfg:
        relevant_node_names = set(reduce(lambda x, y: x + y, cfg["edges"], []))
    else:
        relevant_node_names = cfg["nodeNames"]
    irrelevant_node_names = set(cfg["nodeNames"]).difference(relevant_node_names)
    if len(irrelevant_node_names) > 0:
        logger.info(f"There are irrelevant nodes in your config: {irrelevant_node_names}")

    relevant_ix = [cfg["nodeNames"].index(name) for name in relevant_node_names]
    for ix in relevant_ix:
        node_type_name = cfg["nodeTypes"][ix]
        node_name = cfg["nodeNames"][ix]
        node_params = {} if not node_name in cfg["hyperParameters"] else cfg["hyperParameters"][node_name]
        try:
            node_type = globals()[node_type_name]
            node_instance = node_type(name=node_name, **node_params)
        except Exception as e:
            logger.debug(f"Node '{node_type_name}' with name: '{node_name}'. Exception {e}.")
            breakpoint()
        nodes.append(node_instance)
    return nodes

def main():
    args = getArgs()
    nodes = getNodes(args.graphCfg)
    model = build_model(nodes, args.graphCfg).to("cpu")
    print(LightningModuleEnhanced(model).summary())

    # callback = PlotResultsEveryEpoch(nodes, on_train_set=True)
    callbacks = [PlotMetrics(), PlotCallbackGeneric(partial(nodes_plot_fn, nodes=nodes))]

    transforms = {node: get_data_transform(node) for node in nodes}
    trainer = NGCTrainer(model=model, ngc_dir_path=args.ngcDir, train_cfg=args.trainCfg, train_dir=args.trainDir,
                        validation_dir=args.validationDir, semisupervised_dirs=args.semisupervisedDirs,
                        transforms=transforms, edge_callbacks=callbacks, augmentation_fn=get_augmentation)
    trainer.run(start_iteration=args.startIteration, end_iteration=args.endIteration,
                parallelization=args.parallelization, debug=args.debug)

if __name__ == "__main__":
    main()
