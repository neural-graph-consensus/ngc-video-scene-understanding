import torch as tr
from nwgraph import MapNode
from ngclib_cv.nodes import Normal as _Normal
from .models import EncoderMap2Map, DecoderMap2Map

class DecoderMap2MapNormal(DecoderMap2Map):
    def forward(self, x):
        y = super().forward(x)
        # Make unit vector
        Norm = tr.norm(y, dim=-1).unsqueeze(-1)
        y = y / Norm
        # Move to [0:1]
        y = (y + 1) / 2
        return y

class Normal(_Normal):
    def get_encoder(self, outputNode):
        assert isinstance(outputNode, MapNode)
        return EncoderMap2Map(dIn=self.num_dims)

    def get_decoder(self, inputNode):
        assert isinstance(inputNode, MapNode)
        return DecoderMap2MapNormal(dOut=self.num_dims)
