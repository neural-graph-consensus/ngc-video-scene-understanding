# Script used to test the metrics given two directories (gt and predictions), as well as a node type and a graph cfg
#  to match the node type to it and get necessary metrics/loss function.
import yaml
from argparse import ArgumentParser
from typing import Dict, OrderedDict, Tuple, List
from pathlib import Path
from nwgraph import Node
from ngclib.utils import load_npz
from ngclib_cv.data import get_data_transform
from nwutils.path import get_files_from_dir
from ngclib.logger import logger
from functools import partial
import torch as tr

from main import getNodes

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--predictionDir", required=True)
    parser.add_argument("--gtDir", required=True)
    parser.add_argument("--nodeName", required=True)
    parser.add_argument("--graphCfgPath", required=True)
    parser.add_argument("--N", type=int)
    args = parser.parse_args()
    args.gtDir = Path(args.gtDir).absolute()
    args.predictionDir = Path(args.predictionDir).absolute()
    args.graphCfg = yaml.safe_load(open(args.graphCfgPath, "r"))
    return args

def get_node(graph_cfg: Dict, node_name: str) -> Node:
    nodes = getNodes(graph_cfg)
    assert node_name in nodes, f"Cannot find '{node_name}' in {nodes}"
    ix = nodes.index(node_name)
    node = nodes[ix]
    return node

def get_data(y_dir: Path, gt_dir: Path, node: Node, N: int = None) -> Tuple[tr.Tensor, tr.Tensor]:
    randomize, seed = None, None
    if N > 0:
        logger.info(f"N set to '{N}', also randomizing with seed 42")
        randomize = True
        seed = 42
    y_paths = get_files_from_dir(y_dir, pattern="*.npz", n_files=N, randomize=randomize, seed=seed)
    gt_paths = get_files_from_dir(gt_dir, pattern="*.npz", n_files=N, randomize=randomize, seed=seed)
    if y_paths[0].name != gt_paths[0].name:
        logger.info("Names mismatch, probably random seeds are not equal, be careful.")
    transform = partial(get_data_transform(node), node=node)

    assert len(y_paths) == len(gt_paths), f"{len(y_paths)} vs {len(gt_paths)}"
    assert len(y_paths) > 0
    logger.info(f"Got {len(y_paths)} paths for node {node}")

    y = map(tr.Tensor, map(load_npz, y_paths))
    gt = map(tr.Tensor, map(transform, map(load_npz, gt_paths)))

    y = tr.stack(list(y))
    gt = tr.stack(list(gt)).reshape(y.shape)
    logger.info(f"Stacked tensors to shape: {y.shape}")
    return y, gt

def get_metrics(y: List[tr.Tensor], gt: List[tr.Tensor], node: Node, res: OrderedDict) -> Dict:
    def format(res):
        res = float(res)
        assert res > 0
        if res < 1:
            return round(res, 6)
        if res < 10:
            return round(res, 5)
        return round(res, 4)

    if not hasattr(node, "offline_metrics"):
        logger.info("Node '{node.name}' has no offline metrics, using online metrics!")
        node.offline_metrics = node.metrics

    res["criterion"] = format(node.criterion(y, gt))
    for metric_name, metric_fn in node.offline_metrics.items():
        res[metric_name] = format(metric_fn(y, gt))
    return res

def node_metrics_loco(y_dir, gt_dir, node, N):
    y, gt = get_data(y_dir, gt_dir, node, N)
    which = "/".join(str(y_dir).split("/")[-2:])
    res = OrderedDict({"name": node.name, "which": which})
    res = get_metrics(y, gt, node, res)
    res["path"] = str(y_dir)
    return dict(res)

def main():
    args = getArgs()
    node = get_node(args.graphCfg, args.nodeName)
    
    out_file = f"{node.name}.txt"
    res = node_metrics_loco(args.predictionDir, args.gtDir, node, args.N)
    open(out_file, "a").write(f"{res}\n")
    logger.info(f"Updated '{out_file}'")

if __name__ == "__main__":
    main()
